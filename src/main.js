import React, { Component } from 'react';
import {
    StyleSheet
} from 'react-native';
import NavigationExperimental from 'react-native-deprecated-custom-components';

var Logining = require('./pages/registration').default;
var Contacts = require('./pages/contacts').default;
var ROUTES = {
    logining: Logining,
    contacts: Contacts
};

export default class Main extends Component {
    _renderScene(route, navigator) {
        var Component = ROUTES[route.name];
        return <Component route={route} navigator={navigator} />;
    }

    render() {
        return (
            <NavigationExperimental.Navigator
                initialRoute={{name: 'logining'}}
                renderScene={this._renderScene}
                configureScene={() => {
                    return NavigationExperimental.Navigator.SceneConfigs.FloatFromRight;
                }}
                style={styles.container}
            />
        );
    }
};

var styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#3498db',
    }
});

/*
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View
} from 'react-native';
import Logining from './src/components/Logining';


export default class TeroTelegram extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Logining/>
                <Text style={styles.instructions}>
                    To get started, edit index.ios.js
                </Text>
                <Text style={styles.instructions}>
                    Press Cmd+R to reload,{'\n'}
                    Cmd+D or shake for dev menu
                </Text>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 20,
        backgroundColor: '#e67e22',
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

AppRegistry.registerComponent('TeroTelegram', () => TeroTelegram);
*/