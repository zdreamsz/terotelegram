import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Text,
    Image
} from 'react-native';

export  default  class ListViewDemo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            number: props.children.number,
            message: props.children.message,
        };
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.containerAva}>
                    <Image style={styles.ava} source={require('../images/ava.png')}/>
                </View>
                <View style={styles.containerNumber}>
                    <Text style={styles.number}>{this.state.number}</Text>
                    <Text style={styles.message}>{this.state.message}</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        padding: 2,
        marginBottom: 20,
    },
    ava: {
        width: 50,
        height: 50,
    },
    containerAva: {
        //backgroundColor: '#ff0',
        margin: 1,
        width: 50
    },
    containerNumber: {
        //backgroundColor: '#f00',
        borderBottomWidth: 0.5,
        borderBottomColor: 'rgba(255,255,255,0.8)',
        marginLeft: 5,
        marginRight: 5,
        width: '83.8%'
    },
    number: {
        color: 'rgba(255,255,255,0.8)',
        fontWeight: 'bold',
        justifyContent: 'flex-start'
    },
    message: {
        paddingTop: 1,
        paddingBottom: 2,
        color: 'rgba(255,255,255,0.5)',
    }
});