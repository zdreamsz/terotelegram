import React, { Component } from 'react';
import {
    View,
    ListView,
    StyleSheet,
    Text
} from 'react-native';
import Chat from '../components/chat';

export  default  class ListViewDemo extends Component {
    constructor(props) {
        super(props);

        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

        this.state = {
            dataSource: ds.cloneWithRows([
                {number: '+380986621869', message: "Привет! Как дела?"},
                {number: '+380986621869', message: "Привет! Как дела?"},
                {number: '+380986621869', message: "Привет! Как дела?"},
                {number: '+380986621869', message: "Привет! Как дела?"},
                {number: '+380986621869', message: "Привет! Как дела?"},
                {number: '+380986621869', message: "Привет! Как дела?"},
                {number: '+380986621869', message: "Привет! Как дела?"},
                {number: '+380986621869', message: "Привет! Как дела?"},
                {number: '+380986621869', message: "Привет! Как дела?"},
                {number: '+380986621869', message: "Привет! Как дела?"},
            ]),
        };
    }

    render() {
        return (
            <View>
                <View style={styles.header}>
                    <Text style={styles.headerText}>
                        Contacts
                    </Text>
                </View>

                <ListView
                    dataSource={this.state.dataSource}
                    renderRow={(data) => <Chat>{data}</Chat>}
                />

            </View>
        );
    }
}

var styles = StyleSheet.create({
    header: {
        backgroundColor: '#2980b9',
        //borderBottomRadius: 4,
        //borderBottomWidth: 0.5,
        //borderBottomColor: '#000',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerText: {
        marginTop: 5,
        fontWeight: 'bold',
        fontSize: 20,
        color: '#fff'
    }
});

// <View style={styles.view}><Text>{data}</Text></View>

