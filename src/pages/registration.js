import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    KeyboardAvoidingView,
    Button,
    TextInput,
    AsyncStorage,
} from 'react-native';

export  default  class Logining extends Component {
    constructor(props) {
        super(props);

        this.state = {
            login: "",
            password: "",
            password_repeat: "",
            access_token: "",
            //temp: 0
        },
        this._onPressConnect = this._onPressConnect.bind(this);
    }


    _onPressConnect() {
        if (this.state.password == "" || this.state.password_repeat == "" || this.state.login == "") {
            alert("Not all fields are filled out!");
            return;
        } else if (this.state.password != this.state.password_repeat) {
            alert("Your repeated passwort is not equal to your password")
            return;
        }

        fetch('http://localhost:3000/authorization', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                login: this.state.login,
                password: this.state.password,
            })
        }).then(function (response) {
            response.json().then(function (data) {
                if (data.error)
                    alert(data.error);
                else {
                    alert("You have successfully registered!");
                    AsyncStorage.setItem('access_token', data.access_token);
                }
            });
        }).catch(function () {
            alert("Not connect!");
            return;
        });
    }

    my() {
        alert(5);
    }

    render() {
        AsyncStorage.getItem('access_token').then((access_token)=> {
            if(access_token !== null /*&& this.state.temp == 0*/)
                this.props.navigator.push({name: 'contacts'});
            //this.state.temp++;
        });

        return (
            <KeyboardAvoidingView behavior="padding" style={styles.GlobalContainer}>
                <View style={styles.WelcomeContainer}>
                    <Text style={styles.text}>
                        Welcome to TeroTelegram!
                    </Text>
                    <Image style={styles.logo} source={require('../images/logo.jpg')}/>
                </View>
                <View style={styles.RegisterContainer}>
                    <TextInput
                        onChangeText={(val) => this.setState({login: val})}

                        placeholder="phone number"
                        placeholderTextColor="rgba(0,0,0,0.5)"
                        style={styles.input}
                        keyboardType="phone-pad"
                        autoCapitalize={'none'}
                        autoCorrect={true}
                    />
                    <TextInput
                        returnKeyType="go"
                        placeholder="password"
                        placeholderTextColor="rgba(0,0,0,0.5)"
                        secureTextEntry
                        style={styles.input}
                        onChangeText={(val) => this.setState({password: val})}
                    />
                    <TextInput
                        returnKeyType="go"
                        placeholder="password repeat"
                        placeholderTextColor="rgba(0,0,0,0.5)"
                        secureTextEntry
                        style={styles.input}
                        onChangeText={(val) => this.setState({password_repeat: val})}
                    />
                    <View style={styles.button}>
                        <Button
                            title="register"
                            onPress={this._onPressConnect}
                            color="rgba(255,255,255,0.9)"
                        />
                    </View>
                </View>
            </KeyboardAvoidingView>
        )
    }
}

const styles = StyleSheet.create({
    GlobalContainer: {
        flex: 1,
        backgroundColor: '#3498db',
        width: '100%',
    },
    WelcomeContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flexGrow: 1
    },
    text: {
        paddingTop: 5,
        fontSize: 20,
        color: '#fff',
        textDecorationLine: 'underline',
        textDecorationStyle: 'double',
        textDecorationColor: '#95a5a6'
    },
    logo: {
        resizeMode: 'center',
        width: 187,
        height: 140,
        opacity: 0.8,
    },
    RegisterContainer: {
        width: '100%',
        padding: 20
    },
    input: {
        marginTop: 15,
        height: 40,
        backgroundColor: 'rgba(255,255,255,0.2)',
        paddingHorizontal: 10,
        borderWidth: 0.5,
        borderColor: '#1b79b7',
        color: '#fff'
    },
    button: {
        backgroundColor: 'rgba(27,121,183,0.5)',
        borderWidth: 0.5,
        borderColor: '#1b79b7',
        marginTop: 15,
        marginBottom: 15,
    },
});